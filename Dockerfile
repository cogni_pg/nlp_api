FROM python:3.6

# Create app directory
WORKDIR /app

# Install app dependencies
COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

RUN python -m spacy download en_core_web_lg
# Bundle app source
COPY . /app

EXPOSE 8080
CMD [ "python", "main.py" ]