import numpy as np
import joblib
import re
import spacy
from spacy.lang.en import English
from spacy.lang.en.stop_words import STOP_WORDS
import string
# import en_core_web_lg
nlp = spacy.load('en_core_web_lg')
# nlp=en_core_web_lg.load()
### import nltk
# from nltk.sentiment.vader import SentimentIntensityAnalyzer
# sid = SentimentIntensityAnalyzer()
# from textblob import TextBlob
from flask import Flask,request,jsonify
# def sentiment(text):
    # return sid.polarity_scores(text)["compound"]


# def getPolarity(text):
    # return  TextBlob(text).sentiment.polarity


def sentiment_class(text):
    svc_from_joblib = joblib.load('filename.pkl') 
    string=text
    tweet=[1]
    tweet[0]=string
    prediction_svc=svc_from_joblib.predict(np.array(tweet))
    return "Positive" if prediction_svc[0]==1 else "Negative"


def clean_text(text):
    text=text.lower()
    text = re.sub('@[A-Za-z0-9]+', '', text) #Removing @mentions
    text = re.sub('#', '', text) # Removing '#' hash tag
    text = re.sub('RT[\s]+', '', text) # Removing Retweets to avoid repitition
    text = re.sub('https?:\/\/\S+', '', text) # Removing hyperlink
    return text


def clean(text):
    doc=nlp(text)
    lemmatized_tokens=[token.lemma_ for token in doc if token.is_stop==False and token.text not in string.punctuation and token.lemma_ !='-PRON-']
    cleaned_text= ' '.join(lemmatized_tokens)
    return cleaned_text

app=Flask(__name__)

@app.route('/')
def home():
    return "welcome to home page"
@app.route('/raw_tweet/',methods=['GET'])
def sentiments():
    if request.method=='GET':
        tweet=request.args.get('tweet',type=str)
        tweet=clean_text(tweet)
        tweet=clean(tweet)
        sentiment_type=sentiment_class(tweet)
        response={}
        response['user_text']=tweet
        response['sentiment_class']=sentiment_type
        return jsonify(response)

@app.route('/entity',methods=['GET'])
def entity():
    text=request.args.get('text',type=str)
    doc=nlp(text)
    entities={}
    for ent in doc.ents:
        entities[ent.text]=spacy.explain(ent.label_)
    return jsonify(entities)


@app.route('/intent',methods=['GET'])
def intent_detect():
    intents=["Give me contact details",
             "Do you offer home delivery service",
             "Do you sell pet food",
             "what is the address of your store",
            "how can you help me",
            "I want a transcript "]
    text=request.args.get('text',type=str)
    text=nlp(text)
    doc_text=nlp(' '.join([str(t) for t in text if not t.is_stop]))
    scores=[]
    for intent in intents:
        doc=nlp(intent)
        doc_no_stop=nlp(' '.join([str(t) for t in doc if not t.is_stop]))
        scores.append(doc_no_stop.similarity(doc_text))
    closest=scores.index(max(scores))
    return intents[closest]

if __name__=='__main__':
    app.run( host='0.0.0.0',port=8080)
